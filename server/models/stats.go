package models

/*
Copyright © 2022 zeroNounours

*/

import (
	"strings"
	"time"
)

// Stat is the struct to store answers to trainings
type Stat struct {
	CardID int64    `json:"cardid"`
	Side   int      `json:"side"`
	Answer bool     `json:"answer"`
	Date   jsonDate `json:"date"`
}

// statCreateTblQuery is the SQL query to use to create the table within the
// SQLite database
const statCreateTblQuery string = `
  CREATE TABLE IF NOT EXISTS stat(
	cardid INTEGER REFERENCES card ON DELETE CASCADE,
  side INTEGER,
  answer INTEGER,
  date DATETIME
  );
`

func init() {
	// register the function to initialize the database
	Models.register(func() error {
		// Create the table if not exist
		_, err := db.Exec(statCreateTblQuery)
		return err
	})
}

// SQLCondition is the type to define conditions for Find
type SQLCondition func() (string, []any)

// StatWithCardID is the func to be used to add a cardId condition on Find()
func StatWithCardID(cardid int) SQLCondition {
	return func() (string, []any) {
		return `cardid = ?`, []any{cardid}
	}
}

// StatWithAnswer is the func to be used to add an Answer condition on Find()
func StatWithAnswer(answer bool) SQLCondition {
	return func() (string, []any) {
		return `answer = ?`, []any{answer}
	}
}

// StatWithDate is the func to be used to add a Date condition on Find()
func StatWithDate(date time.Time) SQLCondition {
	return func() (string, []any) {
		return `date = ?`, []any{date.Format("2006-01-02")}
	}
}

// Find is the method to get stats filtered based on arguments
// It can be given any number of SQLCondition to filter them
func (s *Stat) Find(conditions ...SQLCondition) ([]Stat, error) {
	// Create the query
	qry := "SELECT cardid, side, answer, date FROM stat"

	// Create WHERE clauses
	var where []string
	var whereargs []any
	for _, cond := range conditions {
		w, wargs := cond()
		where = append(where, w)
		for _, arg := range wargs {
			whereargs = append(whereargs, arg)
		}
	}

	// append the WHERE clause to the query
	if len(where) > 0 {
		qry += ` WHERE ` + strings.Join(where, ` AND `)
	}

	// Execute the query to the database
	rows, err := db.Query(qry, whereargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Scan the results
	ret := []Stat{}
	for rows.Next() {
		var stat Stat
		if err := rows.Scan(
			&stat.CardID,
			&stat.Side,
			&stat.Answer,
			&stat.Date.Time,
		); err != nil {
			return nil, err
		}
		ret = append(ret, stat)
	}

	// return all IDs
	return ret, nil
}

// Save is the method to save a new stat into database
func (s *Stat) Save() error {
	// Execute the query to the database
	_, err := db.Exec(
		`INSERT INTO stat (cardid, side, answer, date)
		VALUES (?, ?, ?, date('now'))`,
		s.CardID,
		s.Side,
		s.Answer,
	)
	if err != nil {
		return err
	}
	return nil
}
