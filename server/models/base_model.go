package models

/*
Copyright © 2022 zeroNounours

*/

import (
	"database/sql"
)

// Model is the interface which should be followed by any Model and should
// registered to the Models structure
type Model interface {
	InitDB(*sql.DB) error
	Save() error
	Get(int64) error
}

// Models is the struct which is used to register all models
type models struct {
	initializers []func() error
	DB           *sql.DB
}

// Models is the instance of type models which holds the common structure for
// all Model
var Models models

var db *sql.DB

// Register is the function any Model must use to register the its initializer
func (ms *models) register(init func() error) error {
	// Append tho the slice of initializers
	ms.initializers = append(ms.initializers, init)

	// Ensure the initializer is called directly if already initialized
	if ms.DB != nil {
		return init()
	}

	return nil
}

// InitDB initialize the database. It opens it and ensure all required tables
// are created
func (ms *models) InitDB(dbpath string) error {
	// Open the database
	sqldb, err := sql.Open("sqlite3", dbpath)
	if err != nil {
		return err
	}

	// save the db for later
	db = sqldb
	ms.DB = sqldb

	// If any model is already registered, initialize it
	for _, init := range ms.initializers {
		if err := init(); err != nil {
			return err
		}
	}

	// No error to return
	return nil
}
