package models

/*
Copyright © 2022 zeroNounours

*/

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
)

// Card is the struct to manage memory card
type Card struct {
	ID           int64     `json:"id"`
	Category     string    `form:"category" json:"category" binding:"required"`
	Side1        string    `form:"side1" json:"side1" binding:"required"`
	Side2        string    `form:"side2" json:"side2" binding:"required"`
	NextSide     int       `form:"next_side" json:"next_side"`
	CurrentDelta int       `json:"-"`
	NextTraining time.Time `json:"-"`
}

// cardCreateTblQuery is the SQL query to use to create the table within the
// SQLite database
const cardCreateTblQuery string = `
  CREATE TABLE IF NOT EXISTS card(
  id INTEGER NOT NULL PRIMARY KEY,
  category TEXT,
  side1 TEXT,
  side2 TEXT,
	current_delta INTEGER,
  next_train DATETIME,
  next_side INTEGER
  );
`

// cardGetQuery is the SQL query to get a card by ID
const cardGetQuery string = `
	SELECT
		id,
		category,
		side1,
		side2,
		current_delta,
		next_train,
		next_side
	FROM card WHERE id=?
`

// cardInsertQuery is the SQL query to use to create a new card
const cardInsertQuery string = `
	INSERT INTO card (category, side1, side2, current_delta, next_train, next_side)
	VALUES (?, ?, ?, 0, date('now', '1 day'), 0)
	RETURNING id
`

// cardUpdateQuery is the SQL query to use to modify a card
const cardUpdateQuery string = `
	UPDATE card SET category=?, side1=?, side2=? WHERE id=?
`

// cardDeleteQuery is the SQL query to use to modify a card
const cardDeleteQuery string = `
	DELETE FROM card WHERE id=?
`

// cardValidateQuery is the SQL query to use to validate/unvalidate a card
const cardValidateQuery string = `
	UPDATE card SET
		current_delta=?,
		next_train=date('now', ? || ' day'),
		next_side=(next_side == 0)
	WHERE id=?
`

func init() {
	// register the function to initialize the database
	Models.register(func() error {
		// Create the table if not exist
		_, err := db.Exec(cardCreateTblQuery)
		return err
	})
}

// GetByID is the method to get a card using its ID
func (c *Card) GetByID(id int64) error {
	// Execute the query to the database
	if err := db.QueryRow(cardGetQuery, id).Scan(
		&c.ID,
		&c.Category,
		&c.Side1,
		&c.Side2,
		&c.CurrentDelta,
		&c.NextTraining,
		&c.NextSide,
	); err != nil {
		if err == sql.ErrNoRows {
			return fmt.Errorf("Entry not found: %d", id)
		}
		return err
	}
	// Create the Struct from the row
	return nil
}

// FindCard is the method to get the IDs of card
// Args:
//
//	train (bool): only return card to be trained
//	categories ([]string): catogories to filter (empty for no filtering)
//	orderby (string): on which column to filter (valid is "category" and
//		"id")
func (c *Card) FindCard(train bool, categories []string, orderby string) ([]int64, error) {
	ret := []int64{}
	// Create the query
	qry := "SELECT id FROM card"

	// Create WHERE clauses
	var where []string
	var whereargs []any
	// train filter
	if train {
		where = append(where, `next_train <= date('now')`)
	}
	// categories filter
	if len(categories) > 0 {
		where = append(where, `category IN (?`+strings.Repeat(`,?`, len(categories)-1)+`)`)
		// copy each category to args
		for _, v := range categories {
			whereargs = append(whereargs, v)
		}
	}

	// append the WHERE clause to the query
	if len(where) > 0 {
		qry += ` WHERE ` + strings.Join(where, ` AND `)
	}

	// add the order by clause
	switch orderby {
	case "category":
		qry += ` ORDER BY category, id`
	case "id":
		qry += ` ORDER BY id`
	default:
		return nil, errors.New("Invalid orderby value (must be category or id)")
	}

	// Execute the query to the database
	rows, err := db.Query(qry, whereargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		ret = append(ret, id)
	}

	// return all IDs
	return ret, nil
}

// GetCard is the method to get the cards based on filters
// This method is similar to FindCard, but it returns the complete card data
// Args:
//
//	train (bool): only return card to be trained
//	categories ([]string): catogories to filter (empty for no filtering)
//	orderby (string): on which column to filter (valid is "category" and
//		"id")
func (c *Card) GetCard(train bool, categories []string, orderby string) ([]Card, error) {
	// Create the query
	qry := `SELECT
		id,
		category,
		side1,
		side2,
		current_delta,
		next_train,
		next_side
		FROM card`

	// Create WHERE clauses
	var where []string
	var whereargs []any
	// train filter
	if train {
		where = append(where, `next_train <= date('now')`)
	}
	// categories filter
	if len(categories) > 0 {
		where = append(where, `category IN (?`+strings.Repeat(`,?`, len(categories)-1)+`)`)
		// copy each category to args
		for _, v := range categories {
			whereargs = append(whereargs, v)
		}
	}

	// append the WHERE clause to the query
	if len(where) > 0 {
		qry += ` WHERE ` + strings.Join(where, ` AND `)
	}

	// add the order by clause
	switch orderby {
	case "category":
		qry += ` ORDER BY category, id`
	case "id":
		qry += ` ORDER BY id`
	default:
		return nil, errors.New("Invalid orderby value (must be category or id)")
	}

	// Execute the query to the database
	rows, err := db.Query(qry, whereargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// create the array to return
	ret := []Card{}
	for rows.Next() {
		var card Card
		if err := rows.Scan(
			&card.ID,
			&card.Category,
			&card.Side1,
			&card.Side2,
			&card.CurrentDelta,
			&card.NextTraining,
			&card.NextSide,
		); err != nil {
			return nil, err
		}
		ret = append(ret, card)
	}

	// return all IDs
	return ret, nil
}

// Save is the method to save or modify the card into database
func (c *Card) Save() error {
	if c.ID == 0 {
		return c.create()
	}
	return c.modify()
}

// Create a new entry
func (c *Card) create() error {
	// Execute the query to the database
	res, err := db.Exec(
		cardInsertQuery,
		c.Category,
		c.Side1,
		c.Side2,
	)
	if err != nil {
		return err
	}

	// Save the latest ID
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	c.ID = id
	return nil
}

// update an existing entry
func (c *Card) modify() error {
	// Execute the query to the database
	if _, err := db.Exec(
		cardUpdateQuery,
		c.Category,
		c.Side1,
		c.Side2,
		c.ID,
	); err != nil {
		return err
	}
	return nil
}

// Delete is the method to delete the card from the database
func (c *Card) Delete() error {
	if c.ID != 0 {
		// Execute the query to the database
		if _, err := db.Exec(cardDeleteQuery, c.ID); err != nil {
			return err
		}
	}
	return nil
}

// getDelta returns the number of days between each training
func getDelta() []int {
	return []int{1, 2, 7, 30, 91, 182, 365}
}

// Validate an existing card
func (c *Card) Validate(isvalid bool) error {
	deltas := getDelta()

	// compute the next delta
	if isvalid && (c.CurrentDelta+1) < len(deltas) {
		c.CurrentDelta = c.CurrentDelta + 1
	} else if !isvalid && c.CurrentDelta > 0 {
		c.CurrentDelta = c.CurrentDelta - 1
	}

	// start a new DB transaction context
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	// Execute the update query to the database
	if _, err := db.Exec(
		cardValidateQuery,
		c.CurrentDelta,
		deltas[c.CurrentDelta],
		c.ID,
	); err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}

	// Create an stat history in the DB
	var stat Stat
	stat.CardID = c.ID
	stat.Answer = isvalid
	stat.Side = c.NextSide
	if err := stat.Save(); err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}

	// Commit and return
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

// GetCategories is the method to get the list of unique categories
func (c *Card) GetCategories() ([]string, error) {
	ret := []string{}
	// Create the query
	qry := "SELECT DISTINCT category FROM card"

	// Execute the query to the database
	rows, err := db.Query(qry)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var cat string
		if err := rows.Scan(&cat); err != nil {
			return nil, err
		}
		ret = append(ret, cat)
	}

	// return all categories
	return ret, nil
}
