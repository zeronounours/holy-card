package models

/*
Copyright © 2022 zeroNounours

*/

import (
	"time"
)

// inspired from https://stackoverflow.com/questions/20475321/override-the-layout-used-by-json-marshal-to-format-time-time
type jsonDate struct {
	time.Time
}

func (j jsonDate) format() string {
	return j.Time.Format("2006-01-02")
}

func (j jsonDate) MarshalText() ([]byte, error) {
	return []byte(j.format()), nil
}

func (j jsonDate) MarshalJSON() ([]byte, error) {
	return []byte(`"` + j.format() + `"`), nil
}
