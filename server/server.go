package server

/*
Copyright © 2022 zeroNounours

*/

import (
	"fmt"
	"log"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"

	"gitlab.com/zeronounours/holy-card/front"
	"gitlab.com/zeronounours/holy-card/server/controllers"
	"gitlab.com/zeronounours/holy-card/server/models"
)

// Router is the gin router object of the server
var Router *gin.Engine

func init() {
	// default to release mode
	gin.SetMode(gin.ReleaseMode)

	// Create the router
	Router = gin.Default()

	// Create a group for API calls
	api := Router.Group("/api")
	api.GET("/health", controllers.HealthController)
	api.POST("/cards", controllers.CardAddController)
	api.GET("/cards", controllers.CardGetController)
	api.GET("/cards/:id", controllers.CardGetIDController)
	api.PATCH("/cards/:id", controllers.CardUpdateController)
	api.DELETE("/cards/:id", controllers.CardDeleteController)
	api.POST("/cards/:id/:action", controllers.CardValidateController)

	api.GET("/stats", controllers.StatGetController)

	api.GET("/categories", controllers.CategoryGetController)

	// Serve the front end as static
	Router.Use(static.Serve("/", front.FrontendFolder()))
}

// Start is the function which start the Gin API web server
func Start(address string, port int, dbpath string) error {
	log.Printf("Open database %s\n", dbpath)
	// Initialize the database
	err := models.Models.InitDB(dbpath)
	if err != nil {
		log.Fatalf("Failed to open the database: %s", err.Error())
	}

	// Start the server
	Router.Run(fmt.Sprintf("%s:%d", address, port))
	return nil
}

// ActivateVerbose enable verbose mode for the Gin server
func ActivateVerbose() {
	// Re-enable verbose mode
	gin.SetMode(gin.DebugMode)
}
