package controllers

/*
Copyright © 2022 zeroNounours

*/

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// HealthController is the function which manages the health endpoint.
// It returns an object with status always true
func HealthController(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status": true,
	})
}
