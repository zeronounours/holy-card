package controllers

/*
Copyright © 2022 zeroNounours

*/

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/zeronounours/holy-card/server/models"
)

// CategoryGetController is the function which retrieve the list of categories
func CategoryGetController(ctx *gin.Context) {
	// retrieve the categories to be returned
	var c models.Card
	ret, err := c.GetCategories()

	// error management
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Return the list as JSON
	ctx.JSON(http.StatusOK, ret)
}
