package controllers

/*
Copyright © 2022 zeroNounours

*/

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/zeronounours/holy-card/server/models"
)

// StatGetController is the function which manages retrieving a list of stats
// By default it returns all stats. The following query params may be
// added to filter stats:
//   - cardid: if present only returns stats associated with the card
//   - answer: if present only returns stats with the given answer
//   - date: if present only returns stats for the single date
func StatGetController(ctx *gin.Context) {
	// first parse the query strings to create SQLConditions
	conditions := []models.SQLCondition{}
	// cardid
	if v := ctx.Query("cardid"); v != "" {
		parsed, err := strconv.Atoi(v)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		conditions = append(conditions, models.StatWithCardID(parsed))
	}
	// answer
	if v := ctx.Query("answer"); v != "" {
		parsed, err := strconv.ParseBool(v)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		conditions = append(conditions, models.StatWithAnswer(parsed))
	}
	// date
	if v := ctx.Query("date"); v != "" {
		parsed, err := time.Parse("2006-01-02", v)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		conditions = append(conditions, models.StatWithDate(parsed))
	}

	// retrieve the stats
	var s models.Stat
	stats, err := s.Find(conditions...)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Return the list as JSON
	ctx.JSON(http.StatusOK, stats)
}
