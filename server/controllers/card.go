package controllers

/*
Copyright © 2022 zeroNounours

*/

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"

	"gitlab.com/zeronounours/holy-card/server/models"
)

// CardAddController is the function which manages creating a card
func CardAddController(ctx *gin.Context) {
	// Decode the card from json
	var json struct {
		Category string `json:"category" bindind:"required"`
		Side1    string `json:"side1" bindind:"required"`
		Side2    string `json:"side2" bindind:"required"`
	}

	if err := ctx.ShouldBindJSON(&json); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create the card from data and save it
	var card models.Card
	card.Category = json.Category
	card.Side1 = json.Side1
	card.Side2 = json.Side2
	if err := card.Save(); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, card)
}

// CardGetController is the function which manages retrieving a list of cards
// By default it returns ID of all cards. The following query params may be
// added to filter/sort card:
//   - train: if present only return card to be trained
//   - category: list (comma-separated) of category to filter
//   - orderby: valid values are category and id
//   - details: if present returns the complete data of cards
func CardGetController(ctx *gin.Context) {
	// first parse the query strings
	train := ctx.Request.URL.Query().Has("train")
	var category []string
	if v := ctx.Query("category"); v != "" {
		category = strings.Split(v, ",")
	}
	orderby := ctx.DefaultQuery("orderby", "id")

	// retrieve card IDs/data to be returned
	var c models.Card
	var ret any
	var err error
	if ctx.Request.URL.Query().Has("details") {
		ret, err = c.GetCard(train, category, orderby)
	} else {
		ret, err = c.FindCard(train, category, orderby)
	}

	// error management
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Return the list as JSON
	ctx.JSON(http.StatusOK, ret)
}

// CardGetIDController is the function which manages retrieving a specific card
func CardGetIDController(ctx *gin.Context) {
	// Retrieve the ID
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(
			http.StatusBadRequest,
			gin.H{"error": fmt.Sprintf("invalid ID: %v (must be an integer)", ctx.Param("id"))},
		)
		return
	}
	// retrieve the card from the ID and return it
	var card models.Card
	err = card.GetByID(int64(id))
	if err != nil {
		ctx.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, card)
}

// CardUpdateController is the function which manages updating a card
func CardUpdateController(ctx *gin.Context) {
	// Retrieve the ID
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(
			http.StatusBadRequest,
			gin.H{"error": fmt.Sprintf("invalid ID: %v (must be an integer)", ctx.Param("id"))},
		)
		return
	}

	// Decode the card from json
	// each entry is optional: only provided entries will be updated
	var json struct {
		Category string `json:"category"`
		Side1    string `json:"side1"`
		Side2    string `json:"side2"`
	}

	if err := ctx.ShouldBindJSON(&json); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// retrieve the card from the ID
	var card models.Card
	err = card.GetByID(int64(id))
	if err != nil {
		ctx.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// update the card based on provided values and save it
	if json.Category != "" {
		card.Category = json.Category
	}
	if json.Side1 != "" {
		card.Side1 = json.Side1
	}
	if json.Side2 != "" {
		card.Side2 = json.Side2
	}
	if err := card.Save(); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, card)
}

// CardDeleteController is the function which manages deleting a card
func CardDeleteController(ctx *gin.Context) {
	// Retrieve the ID
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(
			http.StatusBadRequest,
			gin.H{"error": fmt.Sprintf("invalid ID: %v (must be an integer)", ctx.Param("id"))},
		)
		return
	}

	// retrieve the card from the ID
	var card models.Card
	err = card.GetByID(int64(id))
	if err != nil {
		ctx.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// delete the card
	if err := card.Delete(); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

// CardValidateController is the function which manages validating or
// unvalidating a card
func CardValidateController(ctx *gin.Context) {
	// Get the ID
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(
			http.StatusBadRequest,
			gin.H{"error": fmt.Sprintf("invalid ID: %v", ctx.Param("id"))},
		)
		return
	}
	// Get the action
	action := ctx.Param("action")
	if action != "valid" && action != "invalid" {
		ctx.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": fmt.Sprintf("invalid action: %v (should be either valid or invalid)", action),
			},
		)
		return
	}
	// validate the card based on the action
	var card models.Card
	card.GetByID(int64(id))
	card.Validate(action == "valid")
	ctx.JSON(http.StatusNoContent, nil)
}
