/*
Copyright © 2022 zeroNounours
*/
package main

import (
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/zeronounours/holy-card/cmd"
)

func main() {
	cmd.Execute()
}
