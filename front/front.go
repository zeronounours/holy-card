package front

/*
Copyright © 2022 zeroNounours

*/

import (
	"embed"
	"io/fs"
	"net/http"

	"github.com/gin-contrib/static"
)

//go:embed dist
var frontend embed.FS

type embedFileSystem struct {
	http.FileSystem
}

func (e embedFileSystem) Exists(prefix string, path string) bool {
	_, err := e.Open(path)
	if err != nil {
		return false
	}
	return true
}

// FrontendFolder is func to get the server file system which serve the frontend
func FrontendFolder() static.ServeFileSystem {
	fsys, err := fs.Sub(frontend, "dist")
	if err != nil {
		panic(err)
	}
	return embedFileSystem{
		FileSystem: http.FS(fsys),
	}
}
