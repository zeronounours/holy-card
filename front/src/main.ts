import { createApp } from "vue";
import App from "./App.vue";
import BootstrapVueNext from "bootstrap-vue-next";

// Import Bootstrap and BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-next/dist/bootstrap-vue-next.css";

// Font awesome
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faThumbsUp,
  faThumbsDown,
  faSquarePlus,
  faTrashCan,
  faPenToSquare,
  faArrowsRotate,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

// I18n
import i18n from "./i18n";

library.add(
  faThumbsUp,
  faThumbsDown,
  faSquarePlus,
  faTrashCan,
  faPenToSquare,
  faArrowsRotate
);

// create app
const app = createApp(App);

// Register external compenent
app
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(BootstrapVueNext)
  .use(i18n);

// Register global properties
app.config.globalProperties.capitalize = function (value: string) {
  if (!value) return "";
  return value.charAt(0).toUpperCase() + value.slice(1);
};

// mount the app
app.mount("#app");
