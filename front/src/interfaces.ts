// This file contains several intefaces which may be used with the application

// Card represent the data of a Card as retrieved from the API
export interface CardI {
  id: number;
  category: string;
  side1: string;
  side2: string;
  next_side: number;
}
