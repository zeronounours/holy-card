package cmd

/*
Copyright © 2022 zeroNounours

*/

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"

	"github.com/spf13/cobra"

	"gitlab.com/zeronounours/holy-card/server"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "holy-card",
	Short: "This program provides memory cards which may be used for training your memory",
	Long: `This application allows you to create cards which are a couple of associated
sentences/words (e.g. a word and it's translation).

You can then train on this cards. Each day it will show you one side of the
card and ask you for the opposite side. It may be asked in either way.
Depending on whether you were right or wrong, it will ask again for the card
less or more frequently.
`,
	Run: func(cmd *cobra.Command, args []string) {
		/*
				if run with no arguments, start the serveur with default value and open
			  the browser to it
		*/

		// Get the binary path
		exe, err := os.Executable()
		if err != nil {
			log.Fatalf("Failed to get executable path: %s", err.Error())
		}

		// start the server in a goroutine
		ch := make(chan bool)
		go func() {
			server.Start("127.0.0.1", 8080, path.Join(path.Dir(exe), "holycard.db"))
			ch <- true
		}()

		// Open the browser
		openbrowser("http://127.0.0.1:8080")

		// wait for the server to be stopped
		<-ch
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}
}
