package cmd

/*
Copyright © 2022 zeroNounours

*/

import (
	"github.com/spf13/cobra"

	"gitlab.com/zeronounours/holy-card/server"
)

var (
	// variables used by flags
	address string
	port    int
	dbpath  string
	debug   bool
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start the API web server",
	Long: `Start the API web server on the given interface and port. It is
possible to modify the local database to use.`,

	RunE: func(cmd *cobra.Command, args []string) error {
		// enable debug if asked for
		if debug {
			server.ActivateVerbose()
		}
		return server.Start(address, port, dbpath)
	},
}

func init() {
	rootCmd.AddCommand(startCmd)

	// Defines the flags to be used
	startCmd.Flags().StringVarP(&address, "address", "a", "127.0.0.1", "Address to bind the server to")
	startCmd.Flags().IntVarP(&port, "port", "p", 8080, "Port to bind the server to")
	startCmd.Flags().StringVarP(&dbpath, "db-path", "d", "./card.db", "Local database file to use")
	startCmd.Flags().BoolVarP(&debug, "verbose", "v", false, "Increase verbosity")
}
