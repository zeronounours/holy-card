# Holy Card

This program provides memory cards which may be used for training your memory.
I based this work on the method of Fabien Olicard's Memory box, even if I'm
pretty sure the training method is not his. For instance, you may use it to
revise vocabulary when learning a foreign language.

This application allows you to create cards which are a couple of associated
sentences/words (e.g. a word and it's translation).

You can then train on this cards. Each day it will show you one side of the
card and ask you for the opposite side. It may be asked in either way.
Depending on whether you were right or wrong, it will ask again for the card
less or more frequently.

## Getting started

## Contributing

All contributions are welcomed. See [CONTRIBUTING.md](CONTRIBUTING.md) for more
details.

For developers, you may consider using [pre-commit](https://pre-commit.com) to
ensure a minimal code-quality before pushing. Start by running:

```
go install golang.org/x/lint/golint@latest
pip install pre-commit
pre-commit install
```

## Authors and acknowledgment

A big thanks to Fabien Olicard whose Memory Box helped me discover this method.
To all French people, you should really buy it.

Authors:

- Gauthier SEBAUX (a.k.a zeroNounours)

## License

Under MIT license
