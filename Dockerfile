#####################################
# Build the frontend
#####################################
FROM docker.io/node:18 as build-front

WORKDIR /app

COPY front/package*.json front/yarn.lock ./
RUN yarn install

COPY front .
RUN npm run build

#####################################
# Build the go binary
#####################################
FROM docker.io/golang:alpine AS build-go
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git gcc musl-dev

WORKDIR $GOPATH/src/gitlab.com/zeronounours/holy-card/
COPY . .
# Fetch dependencies.
# Using go get.
RUN go get -d -v
# Copy the frontend from the previous stage
COPY --from=build-front /app/dist front/dist
# Build the binary.
# since new version of git, golang vcs stamping will fail due to different
# owner ==> need to add safe.directory
RUN git config --global --add safe.directory "$(pwd)" \
	&& CGO_ENABLED=1 GOOS=linux go build -a -ldflags '-linkmode external -extldflags "-static"' -o /go/bin/holy-card

#####################################
# Create the final image
#####################################
FROM scratch
# Copy our static executable.
COPY --from=build-go /go/bin/holy-card /bin/holy-card
# Run the hello binary.
ENTRYPOINT ["/bin/holy-card"]
